from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS

app = Flask(__name__)
api = Api(app)
CORS(app)

class Sumar(Resource):
	def post(self):
		one=request.json['one']
		two=request.json['two']
		resultado=int(one)+int(two)
		return {'result':resultado}

api.add_resource(Sumar, '/sumar')  # Route_1

if __name__ == '__main__':
    app.run(host='0.0.0.0')
    app.run(port='5000')
