
function sumar(){
	$('#operador').html("<br><h3>+</h3>");
	var none = $('#numberone')[0].value;
	var ntwo = $('#numbertwo')[0].value;
	var settings = {
		"url": "http://127.0.0.1:8280/sumar/1",
		"headers": {
			"Content-Type": "application/json",
			"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik5UZG1aak00WkRrM05qWTBZemM1TW1abU9EZ3dNVEUzTVdZd05ERTVNV1JsWkRnNE56YzRaQT09In0.eyJhdWQiOiJodHRwOlwvXC9vcmcud3NvMi5hcGltZ3RcL2dhdGV3YXkiLCJzdWIiOiJhZG1pbkBjYXJib24uc3VwZXIiLCJhcHBsaWNhdGlvbiI6eyJvd25lciI6ImFkbWluIiwidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsInRpZXIiOiJVbmxpbWl0ZWQiLCJuYW1lIjoiQ2FsYyIsImlkIjozLCJ1dWlkIjpudWxsfSwic2NvcGUiOiJhbV9hcHBsaWNhdGlvbl9zY29wZSBkZWZhdWx0IiwiaXNzIjoiaHR0cHM6XC9cL2xvY2FsaG9zdDo5NDQzXC9vYXV0aDJcL3Rva2VuIiwidGllckluZm8iOnsiVW5saW1pdGVkIjp7InRpZXJRdW90YVR5cGUiOiJyZXF1ZXN0Q291bnQiLCJzdG9wT25RdW90YVJlYWNoIjp0cnVlLCJzcGlrZUFycmVzdExpbWl0IjowLCJzcGlrZUFycmVzdFVuaXQiOm51bGx9fSwia2V5dHlwZSI6IlBST0RVQ1RJT04iLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImNhcmJvbi5zdXBlciIsIm5hbWUiOiJhcGlfZmxhc2tfc3VtIiwiY29udGV4dCI6Ilwvc3VtYXJcLzEiLCJwdWJsaXNoZXIiOiJhZG1pbiIsInZlcnNpb24iOiIxIiwic3Vic2NyaXB0aW9uVGllciI6IlVubGltaXRlZCJ9XSwiY29uc3VtZXJLZXkiOiJoU0ZXM1U2RnN1WlgyeXZscE83aEozeEo5NDRhIiwiZXhwIjozNzM0Nzk3Mzc4LCJpYXQiOjE1ODczMTM3MzEsImp0aSI6IjY0ZWUwYTIzLTVkNmMtNGQyNC04ZTk2LWZmZTMwYzAzODI2YyJ9.ctzQ_F2WoGsbgYnME7qXLkwWTzkG56nVNOqyAs3oAtxLzJ66QV2qakurQs8roxQV_er1t32pSQVr9dCkXByie-QnrLgqmWx1M2OUvtSyg2L845zMFt2Vlh0imL5O6utXEur0uYFk9ykAouSDJu2W3uyE6GYAcZ8Dc5OWS2rju36RPG40rtL__xnoVddFpxAR7DA-nk21KLjFzriRRdjn9qk7jvj-lYtIJnnvcIW9xBnsW5PLWHWR564hfirxxSm3XZV_E6Y37ngdk8YV6RctgZjarMASPzyjSitDCByAVB8_BOr_aqUxTwFKgBy90I1hLePbtTDAmU3TSH9VQzFiTw"
		  },
		"crossDomain": "true",		
		"method": "POST",
		"timeout": 0,		
		"data": JSON.stringify({"one":none,"two":ntwo}),
	  };
	  
	  $.ajax(settings).done(function (response) {
		$('#resultado')[0].value=response.result;
		console.log(response);
	  });
}
function restar(){
	$('#operador').html("<br><h3>-</h3>");
	var none = $('#numberone')[0].value;
	var ntwo = $('#numbertwo')[0].value;
	var settings = {
		"url": "http://127.0.0.1:8280/restar/1",
		"headers": {
			"Content-Type": "application/json",
			"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik5UZG1aak00WkRrM05qWTBZemM1TW1abU9EZ3dNVEUzTVdZd05ERTVNV1JsWkRnNE56YzRaQT09In0.eyJhdWQiOiJodHRwOlwvXC9vcmcud3NvMi5hcGltZ3RcL2dhdGV3YXkiLCJzdWIiOiJhZG1pbkBjYXJib24uc3VwZXIiLCJhcHBsaWNhdGlvbiI6eyJvd25lciI6ImFkbWluIiwidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsInRpZXIiOiJVbmxpbWl0ZWQiLCJuYW1lIjoiQ2FsY3JlcyIsImlkIjozMywidXVpZCI6bnVsbH0sInNjb3BlIjoiYW1fYXBwbGljYXRpb25fc2NvcGUgZGVmYXVsdCIsImlzcyI6Imh0dHBzOlwvXC9sb2NhbGhvc3Q6OTQ0M1wvb2F1dGgyXC90b2tlbiIsInRpZXJJbmZvIjp7IlVubGltaXRlZCI6eyJ0aWVyUXVvdGFUeXBlIjoicmVxdWVzdENvdW50Iiwic3RvcE9uUXVvdGFSZWFjaCI6dHJ1ZSwic3Bpa2VBcnJlc3RMaW1pdCI6MCwic3Bpa2VBcnJlc3RVbml0IjpudWxsfX0sImtleXR5cGUiOiJQUk9EVUNUSU9OIiwic3Vic2NyaWJlZEFQSXMiOlt7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJjYXJib24uc3VwZXIiLCJuYW1lIjoiYXBpX2ZsYXNrX3JlcyIsImNvbnRleHQiOiJcL3Jlc3RhclwvMSIsInB1Ymxpc2hlciI6ImFkbWluIiwidmVyc2lvbiI6IjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn1dLCJjb25zdW1lcktleSI6InRTcHdkNFdWVWdfcGEyZTZnYzRkSmZjeFZTVWEiLCJleHAiOjM3MzQ4MDYyODIsImlhdCI6MTU4NzMyMjYzNSwianRpIjoiNzk2NzFhZTYtMGZlYi00ZDg5LWJmNDQtMWIzMjIyYWRmZWY5In0.iu9YqFOtFDqFUrg_8Vo7MV3hWVjKHQA0thlzVyFEZKnH8TybPJzUGX-CcdpKd0n0cBcPQokaD9P8eTSKs6_Y74Mx-GttwzS1gEfyDh-j8pWMtQkrHb-xIW47EV1o1w2lCI34B790S2ZD_1PA4CgqvNI-qUjHK2CVFEBYrBYW3STpnqTNhS9cwftjIkCHWbhQL6FWIyr9bwa1xTInT-6TSiHgonwjQCW0JCaC3wxhtVL8sMRtzU8W-z1IflKup1jxXI-rC3zNQV2vEiC3Hq8kvkk1bv2oXnDDlTsM7eeDBEaFO5VS6g4S4LZnCIjm_l8lwakWkq4JCNZPb49X-lWlSg"
		  },
		"crossDomain": "true",		
		"method": "POST",
		"timeout": 0,		
		"data": JSON.stringify({"one":none,"two":ntwo}),
	  };
	  
	  $.ajax(settings).done(function (response) {
		$('#resultado')[0].value=response.result;
		console.log(response);
	  });
}
function multiplicar(){
	$('#operador').html("<br><h3>*</h3>");
	var none = $('#numberone')[0].value;
	var ntwo = $('#numbertwo')[0].value;
	var settings = {
		"url": "http://127.0.0.1:8280/multiplicar/1",
		"headers": {
			"Content-Type": "application/json",
			"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik5UZG1aak00WkRrM05qWTBZemM1TW1abU9EZ3dNVEUzTVdZd05ERTVNV1JsWkRnNE56YzRaQT09In0.eyJhdWQiOiJodHRwOlwvXC9vcmcud3NvMi5hcGltZ3RcL2dhdGV3YXkiLCJzdWIiOiJhZG1pbkBjYXJib24uc3VwZXIiLCJhcHBsaWNhdGlvbiI6eyJvd25lciI6ImFkbWluIiwidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsInRpZXIiOiJVbmxpbWl0ZWQiLCJuYW1lIjoiY2FsY211bHQiLCJpZCI6MzQsInV1aWQiOm51bGx9LCJzY29wZSI6ImFtX2FwcGxpY2F0aW9uX3Njb3BlIGRlZmF1bHQiLCJpc3MiOiJodHRwczpcL1wvbG9jYWxob3N0Ojk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiUFJPRFVDVElPTiIsInN1YnNjcmliZWRBUElzIjpbeyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoiY2FyYm9uLnN1cGVyIiwibmFtZSI6ImFwaV9mbGFza19tdWx0IiwiY29udGV4dCI6IlwvbXVsdGlwbGljYXJcLzEiLCJwdWJsaXNoZXIiOiJhZG1pbiIsInZlcnNpb24iOiIxIiwic3Vic2NyaXB0aW9uVGllciI6IlVubGltaXRlZCJ9XSwiY29uc3VtZXJLZXkiOiJyNXZRRHdHcjR3c1BPV2NUVmwyU1dDV09nRWdhIiwiZXhwIjozNzM0ODA2NTI5LCJpYXQiOjE1ODczMjI4ODIsImp0aSI6Ijk0MjkyMTdkLTUyZDgtNDhkMS04ZmNjLWE1MWNhZmY4ZTRhMiJ9.M3Lja0_5AMC8gNIZcSTDrZLEM8L4aQWKcuurL_coVsXDOo9uAPaDW6tlINjVQf4RgJoZssfYlEO6Ae_K3HiD1gHxc2vpS2PgJG_lCfVbwHKtClinZHEkrprmi_NX9Spy2pQKxuWtV6PNNH63iCUgr1niO5k7kYpyYZjo1Be9ciPYs3snO_7a-DCQuCtW8XtOJe0Gy944ZMoRV_eqRUVJqqoSBFRSm4DANiQfDxW64wj3mITszhUoyIY71rq3mAo4OTvgYpiC4xAuJ0BpJNVsjDNTsm_mQ1t2KeNxFeUPowjDRF6oHrUvWGU9ZSThsegbCtT3LMO8mTZx1XWp3jd9Wg"
		  },
		"crossDomain": "true",		
		"method": "POST",
		"timeout": 0,		
		"data": JSON.stringify({"one":none,"two":ntwo}),
	  };
	  
	  $.ajax(settings).done(function (response) {
		$('#resultado')[0].value=response.result;
		console.log(response);
	  });
}
function dividir(){
	$('#operador').html("<br><h3>/</h3>");
	var none = $('#numberone')[0].value;
	var ntwo = $('#numbertwo')[0].value;
	if(ntwo<=0){ 
		alert("division por 0 N/A");
	}else{
		var settings = {
			"url": "http://127.0.0.1:8280/dividir/1",
			"headers": {
				"Content-Type": "application/json",
				"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik5UZG1aak00WkRrM05qWTBZemM1TW1abU9EZ3dNVEUzTVdZd05ERTVNV1JsWkRnNE56YzRaQT09In0.eyJhdWQiOiJodHRwOlwvXC9vcmcud3NvMi5hcGltZ3RcL2dhdGV3YXkiLCJzdWIiOiJhZG1pbkBjYXJib24uc3VwZXIiLCJhcHBsaWNhdGlvbiI6eyJvd25lciI6ImFkbWluIiwidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsInRpZXIiOiJVbmxpbWl0ZWQiLCJuYW1lIjoiY2FsY2RpdiIsImlkIjozNSwidXVpZCI6bnVsbH0sInNjb3BlIjoiYW1fYXBwbGljYXRpb25fc2NvcGUgZGVmYXVsdCIsImlzcyI6Imh0dHBzOlwvXC9sb2NhbGhvc3Q6OTQ0M1wvb2F1dGgyXC90b2tlbiIsInRpZXJJbmZvIjp7IlVubGltaXRlZCI6eyJ0aWVyUXVvdGFUeXBlIjoicmVxdWVzdENvdW50Iiwic3RvcE9uUXVvdGFSZWFjaCI6dHJ1ZSwic3Bpa2VBcnJlc3RMaW1pdCI6MCwic3Bpa2VBcnJlc3RVbml0IjpudWxsfX0sImtleXR5cGUiOiJQUk9EVUNUSU9OIiwic3Vic2NyaWJlZEFQSXMiOlt7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJjYXJib24uc3VwZXIiLCJuYW1lIjoiYXBpX2ZsYXNrX2RpdiIsImNvbnRleHQiOiJcL2RpdmlkaXJcLzEiLCJwdWJsaXNoZXIiOiJhZG1pbiIsInZlcnNpb24iOiIxIiwic3Vic2NyaXB0aW9uVGllciI6IlVubGltaXRlZCJ9XSwiY29uc3VtZXJLZXkiOiJRMlpCSGlUZ2ZwRmFZbjJTcGFBYkR1ak5idlVhIiwiZXhwIjozNzM0ODA5MDYxLCJpYXQiOjE1ODczMjU0MTQsImp0aSI6IjM0MDBlYjk3LTYyMjItNDg4MC1iMGMyLTE1MThiY2RkZTUyYyJ9.b9I8paSYXm02h3OOcUD1bNyYc813f7w-P6hfqL_dwxKRb6mRyLTYVzVGDgJo6hTJNqumXSu4e1HsgiySz07jC7KuD_ecqKaiLfCFxdpOBAjUA895IHxLLYDZb7wxLJLPwCVvxrwmGx_RaSt1GuYdpvnvwPPGnqFWHZx5lNAfgBxoP5V2ycFikS40Lq0xoFnFQz7m3y2STA3iiDdyjmk-gwdtqyTSxNT4uR-j_eZrmCcyQwNRiSnLR22GXQETIkApZ7Av75JRToOsEut5-SV1fT7okhRmOaJfvmPPfDSWhflrjUfZiaJDmxudanXtFw4NMWI5N6RjvunvhUk0kEjLeA"
			  },
			"crossDomain": "true",		
			"method": "POST",
			"timeout": 0,		
			"data": JSON.stringify({"one":none,"two":ntwo}),
		  };
		  
		  $.ajax(settings).done(function (response) {
			$('#resultado')[0].value=response.result;
			console.log(response);
		  });
	}
}
