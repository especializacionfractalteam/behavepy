from behave import *
import requests
import json

@given('a {numbers} to sum')
def step_impl(context, numbers):
    context.numbers = numbers.split(',')
    context.api_url = 'http://localhost:5000/sumar'
    print('url :'+context.api_url)
	
	
@when('the calculator sums the numbers')
def step_impl(context):
    response = requests.post(url=context.api_url, headers={'content-type': 'application/json'}, data=json.dumps({'one':context.numbers[0],'two':context.numbers[1]}))
    print(response.text)
    context.jsonload = json.loads(response.text)
    context.total = context.jsonload['result']

@then('the {total:d} of sum is correct')
def step_impl(context, total):
    if context.total == total:
    	print("Suma Correcta")
    else:
    	print("Error en la suma")


@given('a {numbers} to res')
def step_impl(context, numbers):
    context.numbers = numbers.split(',')
    context.api_url = 'http://localhost:5001/restar'
    print('url :'+context.api_url)

@when('the calculator res the numbers')
def step_impl(context):
    response = requests.post(url=context.api_url, headers={'content-type': 'application/json'}, data=json.dumps({'one':context.numbers[0],'two':context.numbers[1]}))
    print(response.text)
    context.jsonload = json.loads(response.text)
    context.total = context.jsonload['result']

@then('the {total:d} of res is correct')
def step_impl(context, total):
    if context.total == total:
    	print("Suma Correcta")
    else:
    	print("Error en la suma")




@given('a {numbers} to mul')
def step_impl(context, numbers):
    context.numbers = numbers.split(',')
    context.api_url = 'http://localhost:5002/multiplicar'
    print('url :'+context.api_url)

@when('the calculator mul the numbers')
def step_impl(context):
    response = requests.post(url=context.api_url, headers={'content-type': 'application/json'}, data=json.dumps({'one':context.numbers[0],'two':context.numbers[1]}))
    print(response.text)
    context.jsonload = json.loads(response.text)
    context.total = context.jsonload['result']

@then('the {total:d} of mul is correct')
def step_impl(context, total):
    if context.total == total:
    	print("Suma Correcta")
    else:
    	print("Error en la suma")




@given('a {numbers} to div')
def step_impl(context, numbers):
    context.numbers = numbers.split(',')
    context.api_url = 'http://localhost:5003/dividir'
    print('url :'+context.api_url)

@when('the calculator div the numbers')
def step_impl(context):
    response = requests.post(url=context.api_url, headers={'content-type': 'application/json'}, data=json.dumps({'one':context.numbers[0],'two':context.numbers[1]}))
    print(response.text)
    context.jsonload = json.loads(response.text)
    context.total = context.jsonload['result']

@then('the {total:d} of div is correct')
def step_impl(context, total):
    if context.total == total:
    	print("Suma Correcta")
    else:
    	print("Error en la suma")

