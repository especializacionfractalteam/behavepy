Feature: Operator

Scenario Outline: Get sum total
  Given a <numbers> to sum
  When the calculator sums the numbers
  Then the <total> of sum is correct

  Examples: numbers
  | numbers         | total |
  | 6,4             | 10    |
  | 2,9             | 11    |
  | 6,6             | 12    |
  | 5,8             | 13    |


Scenario Outline: Get res total
  Given a <numbers> to res
  When the calculator res the numbers
  Then the <total> of res is correct

  Examples: numbers
  | numbers          | total |
  | 1,1              | 0     |
  | 5,4              | -1    |
  | 8,6              | -2    |

Scenario Outline: Get mul total
  Given a <numbers> to mul
  When the calculator mul the numbers
  Then the <total> of mul is correct

  Examples: numbers
  | numbers         | total |
  | 5,1             | 5     |
  | 5,2             | 10    |
  | 5,3             | 15    |

Scenario Outline: Get div total
  Given a <numbers> to div
  When the calculator div the numbers
  Then the <total> of div is correct

  Examples: numbers
  | numbers         | total |
  | 10,2           | 5     |
  | 51,3           | 17    |
  | 12,6           | 2     |
  | 120,5          | 24    |
  | 5,2            | 2     |

 
